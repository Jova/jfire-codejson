package link.jfire.codejson.methodinfo.impl.read.array;

import java.lang.reflect.Method;
import link.jfire.codejson.strategy.ReadStrategy;
import link.jfire.codejson.util.NameTool;

public class SetCustomArrayMethodInfo extends AbstractArrayReadMethodInfo
{
    
    public SetCustomArrayMethodInfo(Method method, ReadStrategy strategy)
    {
        super(method, strategy);
    }
    
    @Override
    protected void readOneDim(String bk)
    {
        str += bk + "array1[i1] = ReaderContext.read(" + NameTool.getRootType(getParamType()).getName() + ".class,jsonArray1.get(i1));\n";
    }
    
}
