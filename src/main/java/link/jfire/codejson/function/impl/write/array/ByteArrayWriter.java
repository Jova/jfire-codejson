package link.jfire.codejson.function.impl.write.array;

import link.jfire.baseutil.collection.StringCache;
import link.jfire.codejson.function.WriterAdapter;

public class ByteArrayWriter extends WriterAdapter
{
    @Override
    public void write(Object field, StringCache cache, Object entity)
    {
        byte[] array = (byte[]) field;
        cache.append('[');
        for (byte each : array)
        {
            cache.append(each).append(',');
        }
        if (cache.isCommaLast())
        {
            cache.deleteLast();
        }
        cache.append(']');
    }
}
