package link.jfire.codejson.function.impl.write.array;

import link.jfire.baseutil.collection.StringCache;
import link.jfire.codejson.function.WriterAdapter;

public class IntArrayWriter extends WriterAdapter
{
    
    @Override
    public void write(Object field, StringCache cache, Object entity)
    {
        int[] array = (int[]) field;
        cache.append('[');
        for (int each : array)
        {
            cache.append(each).append(',');
        }
        if (cache.isCommaLast())
        {
            cache.deleteLast();
        }
        cache.append(']');
    }
    
}
