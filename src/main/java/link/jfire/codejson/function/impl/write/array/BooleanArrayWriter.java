package link.jfire.codejson.function.impl.write.array;

import link.jfire.baseutil.collection.StringCache;
import link.jfire.codejson.function.WriterAdapter;

public class BooleanArrayWriter extends WriterAdapter
{
    
    @Override
    public void write(Object field, StringCache cache, Object entity)
    {
        boolean[] array = (boolean[]) field;
        cache.append('[');
        for (boolean each : array)
        {
            cache.append(each).append(',');
        }
        if (cache.isCommaLast())
        {
            cache.deleteLast();
        }
        cache.append(']');
    }
    
}
