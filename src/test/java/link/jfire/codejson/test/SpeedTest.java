package link.jfire.codejson.test;

import link.jfire.baseutil.time.Timewatch;
import link.jfire.codejson.JsonTool;
import link.jfire.codejson.test.simple.Home;
import org.junit.Ignore;
import org.junit.Test;
import com.alibaba.fastjson.JSON;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class SpeedTest extends Support
{
    @Test
//    @Ignore
    public void writeSpeedTest() throws JsonProcessingException
    {
        logger.debug("codejson的输出是\n\n{}\n\n", JsonTool.write(data));
        ObjectMapper mapper = new ObjectMapper();
        mapper.writeValueAsString(data);
        Timewatch timewatch = new Timewatch();
        int count = 1000000;
        timewatch.start();
        for (int i = 0; i < count; i++)
        {
            JSON.toJSONString(data);
        }
        timewatch.end();
        logger.info("fastjson输出耗时：{}", timewatch.getTotal());
        timewatch.start();
        for (int i = 0; i < count; i++)
        {
            JsonTool.write(data);
        }
        timewatch.end();
        logger.info("codejson输出耗时：{}", timewatch.getTotal());
        timewatch.start();
        for (int i = 0; i < count; i++)
        {
            mapper.writeValueAsString(data);
        }
        timewatch.end();
        logger.info("jackson2输出耗时：{}", timewatch.getTotal());
    }
    
    @Test
    @Ignore
    public void writeSpeedTest2()
    {
        Home home = new Home();
        Timewatch timewatch = new Timewatch();
        int count = 1000000;
        JsonTool.write(home);
        timewatch.start();
        for (int i = 0; i < count; i++)
        {
            JSON.toJSONString(home);
        }
        timewatch.end();
        logger.info("简单测试fastjson输出耗时：{}", timewatch.getTotal());
        timewatch.start();
        for (int i = 0; i < count; i++)
        {
            JsonTool.write(home);
        }
        timewatch.end();
        logger.info("简单测试codejson输出耗时：{}", timewatch.getTotal());
        
    }
    
}
